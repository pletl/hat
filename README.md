# Hat

**H**omework **A**ssignement **T**emplate


Eine LaTeX-Vorlage für Hausarbeiten am Institut für Informatik. 

## Features

Einfache Konfiguration der Daten des Autors und der Vorlesung.

Automatische Selbstständigkeitserklärung

## Benutzung

Grundlegende Kenntnisse in LaTeX werden vorausgesetzt.

Öffne document.tex und editiere doch die Daten zur Person, die die Hausarbeit abgibt und zur Vorlesung.

Ersetze die Datei `unterschrift.png` durch eine Datei die deine Unterschrift enthält.

Das wars!

Jetzt ist nur noch die Hausarbeit zu lösen.